Demo video: https://youtu.be/SaaGr0QCzT4

This is a work-in-progress build of a 3D puzzle-solving hack-and-slash game, set in a medieval environment with a horror background.
Implemented in Unreal Engine 4.26.

Currently, some puzzle elements, such as pulling levers, collecting keys and other items to unlock doors have been implemented.

The build is included in: \Build\MedievalMarauder1.0\WindowsNoEditor\

Controls:

Move: W/A/S/D or Arrow keys

Camera: Mouse movement

Jump: Space bar

Slice attack: Left mouse button

Stab attack: Middle mouse button

Block: Right mouse button

Sheathe/Unsheathe: F

Change weapon: T

Sprint: Left shift

Interact with objects: E

Crouch: M

Open/Close Inventory: I

Save game: X

Delete save: C